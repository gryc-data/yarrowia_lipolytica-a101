## *Yarrowia lipolytica* A101

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEB14097](https://www.ebi.ac.uk/ena/browser/view/PRJEB14097)
* **Assembly accession**: [GCA_900087985.1](https://www.ebi.ac.uk/ena/browser/view/GCA_900087985.1)
* **Original submitter**: 

### Assembly overview

* **Assembly level**: Scaffold
* **Assembly name**: YALIA101
* **Assembly length**: 20,590,669
* **#Scaffolds**: 29
* **Mitochondiral**: No
* **N50 (L50)**: 2,267,247 (4)

### Annotation overview

* **Original annotator**: 
* **CDS count**: 6576
* **Pseudogene count**: 158
* **tRNA count**: 514
* **rRNA count**: 113
* **Mobile element count**: 88
