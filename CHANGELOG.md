# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.2 (2021-05-18)

### Edited

* Delete all standard_name qualifiers.

## v1.1 (2021-05-18)

### Edited

* Build feature hierarchy.

### Fixed

* Locus tag issue: YALIA101_S06E08900T.
* Bad mRNA and gene coordinates: YALIA101_S01E12200G.
* Bad mRNA and gene coordinates: YALIA101_S01E29712G.
* Too large frameshift: YALIA101_S01E18778G
* Bad mRNA and gene coordinates: YALIA101_S04E08284G.

## v1.0 (2021-05-18)

### Added

* The 29 annotated scaffolds of Yarrowia lipolytica a101 (source EBI, [GCA_900087985.1](https://www.ebi.ac.uk/ena/browser/view/GCA_900087985.1)).
